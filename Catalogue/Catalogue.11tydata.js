const glob = require('fast-glob')
const { dirname, join } = require('path')
const { stat } = require('fs/promises')
const getColors = require('get-image-colors')
const md = require('markdown-it')()

const { format } = Intl.NumberFormat('fr-FR', {
  style: 'unit',
  unit: 'megabyte',
  maximumFractionDigits: 2
})

const DEFAULT_COLOURS = {
  Fond: '#CCC',
  PageTitres: 'inherit',
  Titre: '#333',
  TitrePremièreLigne: 'inherit',
  Texte: '#333',
}

module.exports = {
  eleventyComputed: {
    layout ({ layout, page }) {
      return page.fileSlug === 'Catalogue' ? layout : 'layout-ouvrage.njk'
    },
    title ({ title, Titre }) {
      return Titre || title
    },
    Auteurice ({ Auteur, Autrice, Auteurs }) {
      return Autrice || Auteur || Auteurs
    },
    AuteuricePrefix ({ Auteur, Autrice }) {
      return (Auteur || Autrice) ? 'par ' : ''
    },
    async Bannière (data) {
      const [filename] = await glob('banniere.{jpg,jpeg,png}', {
        cwd: dirname(data.page.inputPath),
        baseNameMatch: true,
        absolute: true,
        caseSensitiveMatch: false
      })

      return filename ?? null
    },
    // On le préfixe de '/' parce que c'est plus facile à manipuler dans les templates
    Couverture ({ CouvertureFilePath }) {
      return CouvertureFilePath ?? ''
    },
    async CouvertureFilePath (data) {
      // On recherche un fichier dans le même répertoire qui s'appelle couverture.jpg, ou couverture.jpeg, ou couverture.png
      const [filename] = await glob('couverture.{jpg,jpeg,png}', {
        cwd: dirname(data.page.inputPath),
        baseNameMatch: true,
        absolute: true,
        caseSensitiveMatch: false
      })

      return filename ?? null
    },
    async Couleurs ({ Couleurs, CouvertureFilePath }) {
      let computedColours = { ...DEFAULT_COLOURS }

      if (!CouvertureFilePath && !Couleurs) {
        return computedColours
      }

      try {
        if (CouvertureFilePath) {
          const [Fond, TitreOuTexte] = await getColors(CouvertureFilePath, { count: 3 })
          computedColours = {
            ...computedColours,
            Fond: Fond.css(),
            Texte: TitreOuTexte.css(),
            Titre: TitreOuTexte.css(),
            TitrePremièreLigne: TitreOuTexte.css(),
          }
        }
      }
      catch (error) {
        console.error(`L'image %s invalide (genre c'est un PNG renommé en JPG, etc.) (%o)`, CouvertureFilePath, error)
      }
      finally {
        // Surcharge manuelle
        return {
          ...computedColours,
          Fond: Couleurs.Fond ?? computedColours.Fond,
          PageTitres: Couleurs.PageTitres ?? computedColours.PageTitres,
          Titre: Couleurs.Titre ?? computedColours.Titre,
          TitrePremièreLigne: Couleurs.TitrePremièreLigne ?? Couleurs.Titre ?? computedColours.TitrePremièreLigne,
          Texte: Couleurs.Texte ?? computedColours.Texte
        }
      }
    },
    async PDF (data) {
      // On recherche un fichier dans le même répertoire qui se termine en .pdf ou .PDF
      // On considère que c'est le fichier PDF qui contient le texte intégral
      // On le préfixe de '/' parce que c'est plus facile à manipuler dans les templates
      const pattern = join(dirname(data.page.inputPath), '*.{pdf,PDF}')
      const [filename] = await glob(pattern)

      if (!filename) {
        return null
      }

      const { size: filesize } = await stat(filename)

      return {
        filename: filename ? `/${filename}` : '',
        filesize: format(filesize / 1000000)
      }
    },
    RésuméFormatté ({ Résumé = '' }) {
      return md.render(Résumé)
    },
    feature (data) {
      if (!data.Latitude || !data.Longitude) {
        return null
      }

      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [data.Longitude, data.Latitude]
        },
        properties: {
          Titre: data.Titre,
          Adresse: data.Adresse,
          url: data.page.url
        }
      }
    }
  }
}
